// self.addEventListener("install", e=>{
//     e.waitUntil(
//         caches.open("static").then(cache=>{
//             return cache.addAll([
//                 "./",
//                 "./resources",
//                 "./pages/index.html",
//                 "./styles/style.css",
//                 "./scripts/script.js"
//             ]);
//         })
//     )
// });
//
// self.addEventListener("fetch", e =>{
//     e.respondWith(
//         caches.match(e.request).then(response =>{
//             console.log(`[Service Worker] Fetching resource: ${e.request.url}`);
//             return response|| fetch(e.request);
//         })
//     )
// });