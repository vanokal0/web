let currentButton;

class Student{
    static studentAmount = 1;
    static studentMap = new Map();

    fullName;
    group;
    gender;
    birthday;
    status = "offline-status";
    id = 0;
    rowID = "undefined";

    //конструктор класу
    constructor(dataArray){
        this.group = dataArray[0];
        this.fullName = dataArray[1];
        this.gender = dataArray[2];
        this.birthday = dataArray[3];
    }
}

//функція додавання інформації про студента
function addStudentData(){
    let newStudent = getUserInput();
    if(newStudent === false){
        return;
    }

    let studentTable = document.getElementById("student-table");

    newStudent.id = Student.studentAmount+1;
    newStudent.rowID = "std-"+(studentTable.rows.length);
    sendPostRequest(newStudent, "add");
}
//функція редагування інформації про студента
function editStudentData() {
    let newStudent = getUserInput();
    if(newStudent === false){
        return
    }
    //отримання рядка для редагування та значень вводу користувача
    let row = currentButton.parentNode.parentNode.parentNode;
    newStudent.rowID = row.id;
    newStudent.id = Student.studentMap.get(row.id).id;
    sendPostRequest(newStudent, "edit");


    //перевизначення комірок рядка
    row.cells[1].innerHTML = newStudent.group;
    row.cells[2].innerHTML = newStudent.fullName;
    row.cells[3].innerHTML = newStudent.gender;
    row.cells[4].innerHTML = newStudent.birthday;
}

//функція виводу інформації про студента у новий рядок
function outputStudentData(newStudent, rowID){
    let outputRow = document.getElementById(rowID);

    outputRow.cells[0].innerHTML = '<td><label><input type="checkbox"></label></td>';
    outputRow.cells[1].innerHTML = newStudent.group;
    outputRow.cells[2].innerHTML = newStudent.fullName;
    outputRow.cells[3].innerHTML = newStudent.gender;
    outputRow.cells[4].innerHTML = newStudent.birthday;
    outputRow.cells[5].innerHTML = '<div class="offline-status"></div>'
    outputRow.cells[6].innerHTML =
        '                        <div class="button-cell">\n' +
        '                            <button onclick="toggleEditVisible(this)" class="control-button table-button" id= aria-label="edit">\n' +
        '                                <svg width="16px" height="16px" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">\n' +
        '                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M20.8477 1.87868C19.6761 0.707109 17.7766 0.707105 16.605 1.87868L2.44744 16.0363C2.02864 16.4551 1.74317 16.9885 1.62702 17.5692L1.03995 20.5046C0.760062 21.904 1.9939 23.1379 3.39334 22.858L6.32868 22.2709C6.90945 22.1548 7.44285 21.8693 7.86165 21.4505L22.0192 7.29289C23.1908 6.12132 23.1908 4.22183 22.0192 3.05025L20.8477 1.87868ZM18.0192 3.29289C18.4098 2.90237 19.0429 2.90237 19.4335 3.29289L20.605 4.46447C20.9956 4.85499 20.9956 5.48815 20.605 5.87868L17.9334 8.55027L15.3477 5.96448L18.0192 3.29289ZM13.9334 7.3787L3.86165 17.4505C3.72205 17.5901 3.6269 17.7679 3.58818 17.9615L3.00111 20.8968L5.93645 20.3097C6.13004 20.271 6.30784 20.1759 6.44744 20.0363L16.5192 9.96448L13.9334 7.3787Z" fill="#0F0F0F"/>\n' +
        '                                </svg>\n' +
        '                            </button>\n' +
        '\n' +
        '                            <button onclick="toggleWarningVisible(this)" class="control-button table-button" aria-label="delete">\n' +
        '                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash3" viewBox="0 0 16 16">\n' +
        '                                    <path d="M6.5 1h3a.5.5 0 0 1 .5.5v1H6v-1a.5.5 0 0 1 .5-.5M11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3A1.5 1.5 0 0 0 5 1.5v1H1.5a.5.5 0 0 0 0 1h.538l.853 10.66A2 2 0 0 0 4.885 16h6.23a2 2 0 0 0 1.994-1.84l.853-10.66h.538a.5.5 0 0 0 0-1zm1.958 1-.846 10.58a1 1 0 0 1-.997.92h-6.23a1 1 0 0 1-.997-.92L3.042 3.5zm-7.487 1a.5.5 0 0 1 .528.47l.5 8.5a.5.5 0 0 1-.998.06L5 5.03a.5.5 0 0 1 .47-.53Zm5.058 0a.5.5 0 0 1 .47.53l-.5 8.5a.5.5 0 1 1-.998-.06l.5-8.5a.5.5 0 0 1 .528-.47M8 4.5a.5.5 0 0 1 .5.5v8.5a.5.5 0 0 1-1 0V5a.5.5 0 0 1 .5-.5"/>\n' +
        '                                </svg>\n' +
        '                            </button>\n' +
        '                        </div>'
    closeComplexPopup()

}

//функція видалення інформації про студента
function deleteStudentData(){
    let studentTable = document.getElementById('student-table');
    let row = currentButton.parentNode.parentNode.parentNode;

    studentTable.deleteRow(row.rowIndex);
    closeWarning();
}

//функція появи вікна додавання
function toggleAddVisible(){
    resetValid();

    let addPopup = document.getElementById('complex-popup');
    let editButton = document.getElementById('edit-button');
    let addButton = document.getElementById('add-button');

    document.getElementById('popup-heading').innerHTML = 'Add student';
    addPopup.classList.toggle("display");

    addButton.style.display = 'inline-block';
    editButton.style.display = 'none';
}

//функція появи вікна редагування
function toggleEditVisible(button){
    resetValid();

    let editPopup = document.getElementById('complex-popup');
    let editButton = document.getElementById('edit-button');
    let addButton = document.getElementById('add-button');

    document.getElementById('popup-heading').innerHTML = 'Edit student';

    editPopup.classList.toggle("display");

    editButton.style.display = 'inline-block';
    addButton.style.display = 'none';

    setPopupData(button.parentNode.parentNode.parentNode);

    currentButton=button;
}
//функція виводу актуальної інформації у спливаюче вікно
function setPopupData(row){
    let currentStudent = Student.studentMap.get(row.id);
    document.getElementById("student-group").value = currentStudent.group;

    let splitName = currentStudent.fullName.split(' ');

    document.getElementById("student-name").value = splitName[0];
    document.getElementById("student-surname").value = splitName[1];
    document.getElementById("student-gender").value = currentStudent.gender;

    let splitBirthday = currentStudent.birthday.split(".");
    document.getElementById("student-birthday").value = splitBirthday[2]+'-'+splitBirthday[1]+'-'+splitBirthday[0];
    document.getElementById("student-id").value = currentStudent.id;
}
//функція появи вікна видалення
function toggleWarningVisible(button){
    let warningPopup = document.getElementById('warning-popup');
    let deletedRow = button.parentNode.parentNode.parentNode;
    let studentName = deletedRow.cells[2].innerHTML;

    document.getElementById('deleted-name').innerHTML = studentName;


    warningPopup.classList.toggle("display");

    currentButton=button
}
//функції закриття спливаючих вікон
function closeWarning(){
    let closedPopup = document.getElementById("warning-popup");
    closedPopup.classList.toggle("display");
}
function closeComplexPopup(){
    let closedPopup = document.getElementById('complex-popup');
    clearTextInput();
    closedPopup.classList.toggle("display");
}
function clearTextInput(){
    document.getElementById("student-group").value = '-';
    document.getElementById("student-name").value = '';
    document.getElementById("student-surname").value = '';
    document.getElementById("student-gender").value = '-';
    document.getElementById("student-birthday").value = '';
    document.getElementById("student-id").value = '';
}

//функція зчитування даних вводу з спливаючого вікна
function getUserInput(){
    let inputArray = [];

    //отримання значень введених користувачем
    inputArray[0] = document.getElementById("student-group").value;
    inputArray[1] = document.getElementById("student-name").value+" "+document.getElementById("student-surname").value;
    inputArray[2] = document.getElementById("student-gender").value;

    //формування стрічки Birthday згідно формату
    let studentBirthday = document.getElementById("student-birthday").value;
    let splitString = studentBirthday.split('-');
    studentBirthday = "";
    for(let i = splitString.length-1; i>0; i--)
    {
        studentBirthday +=splitString[i]+'.';
    }
    studentBirthday += splitString[0];
    inputArray[3] = studentBirthday;

    return new Student(inputArray);
}

//функція виводу повідомлень валідації
function validateForm(validationResult){
    resetValid();

    //валідація вводу ім'я
    if(validationResult === "name_empty"){
        makeInvalid(document.getElementById("student-name"));
        document.getElementById("validation-error").innerHTML = "Name field is empty!";
        return false;

    }
    if(validationResult === "name_invalid"){
        makeInvalid(document.getElementById("student-name"));
        document.getElementById("validation-error").innerHTML = "Name field value is invalid!";
        return false;

    }

    //валідація вводу прізвища
    if(validationResult === "surname_empty"){
        makeInvalid(document.getElementById("student-surname"));
        document.getElementById("validation-error").innerHTML = "Surname field is empty!";
        return false;
    }
    if(validationResult === "surname_invalid"){
        makeInvalid(document.getElementById("student-surname"));
        document.getElementById("validation-error").innerHTML = "Surname field value is invalid!";
        return false;

    }

    //валідація вводу статі
    if(validationResult === "gender_empty"){
        makeInvalid(document.getElementById("student-gender"));
        document.getElementById("validation-error").innerHTML = "Gender field is empty!";
        return false;
    }

    //валідація вводу дати
    if(validationResult === "date_invalid"){
        makeInvalid(document.getElementById("student-birthday"));
        document.getElementById("validation-error").innerHTML = "Birthday field value is invalid!";
        return false;
    }
    if(validationResult === "date_empty"){
        makeInvalid(document.getElementById("student-birthday"));
        document.getElementById("validation-error").innerHTML = "Birthday field is empty!";
        return false;
    }
    return true;
}

//допоміжні функції для оформлення валідних/невалідних полів
function makeInvalid(invalidField){
    if(!invalidField.classList.contains("invalid-field")){
        invalidField.classList.toggle("invalid-field");
    }
}
function resetValid(){
    let inputFields = document.getElementsByClassName('input-field');

    for(let i =0; i<inputFields.length; i++){
        inputFields[i].classList.remove("invalid-field");
    }
    document.getElementById("validation-error").innerHTML = "";

}

//функція відправлення GET-запиту з інформацією про зміненого студента
function sendGetRequest(newStudent){
    let xhttp = new XMLHttpRequest();
    xhttp.open("GET", "../temp.txt", true);
    xhttp.setRequestHeader('Content-Type', 'application/json');
    let requestBody = "temp.txt?"+newStudent.toString();

    xhttp.send(requestBody); console.log(requestBody);
}

//функція відправлення POST-запиту для операцій ADD/EDIT
function sendPostRequest(newStudent, type){
    let xhttp = new XMLHttpRequest();
    let url = "http://localhost:8080/table/students"
    if(type === "edit"){
        url+= "/"+newStudent.id;
    }

    xhttp.open("POST", url);
    xhttp.setRequestHeader('Content-Type', 'application/json');
    let requestBody = JSON.stringify(newStudent);

    xhttp.onreadystatechange = function (){
        if(xhttp.readyState === XMLHttpRequest.DONE){
            let response = xhttp.responseText;
            console.log("RESPONSE: " + response);
            Student.studentMap.set(newStudent.rowID, newStudent);
            if(xhttp.status === 201){
                if(type === "add"){
                    Student.studentAmount++;
                    let outputTable = document.getElementById("student-table");
                    let newRow = outputTable.insertRow(outputTable.rows.length);
                    newRow.setAttribute("id", newStudent.rowID);
                    for(let i=0; i<7; i++){newRow.insertCell(i);}
                    outputStudentData(newStudent, newRow.id);
                }
                else{
                    outputStudentData(newStudent, newStudent.rowID);
                }
            }
            if(xhttp.status === 406){
                validateForm(response);
            }
        }
    }
    xhttp.send(requestBody); console.log("REQUEST: " + requestBody);
}

if("serviceWorker" in navigator){
    navigator.serviceWorker.register("../sw.js").then(registration=>{
        console.log("SW Registered");
        console.log(registration);
    }).catch(error=>{
        console.log("SW Registration Failed!");
        console.log(error);
    })
}